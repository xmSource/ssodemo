slice v：切片 n：部分
slicing n：切片
involke v：调用

// uiview层次
- (void)dumpView:(UIView *)aView atIndent:(int)indent into:(NSMutableString *)outstring  
{  
    for (int i = 0; i < indent; i++) [outstring appendString:@"--"];  
    [outstring appendFormat:@"[%2d] %@\n", indent, [[aView class] description]];  
    for (UIView *view in [aView subviews])  
        [self dumpView:view atIndent:indent + 1 into:outstring];  
}  
  
// Start the tree recursion at level 0 with the root view  
- (NSString *) displayViews: (UIView *) aView  
{  
    NSMutableString *outstring = [[NSMutableString alloc] init];  
    [self dumpView: self.window atIndent:0 into:outstring];  
    return outstring ;  
}  
// Show the tree  
- (void)logViewTreeForMainWindow  
{  
    //  CFShow([self displayViews: self.window]);  
    NSLog(@"The view tree:\n%@", [self displayViews:self.window]);  
} 