//
//  ViewController.m
//  SsoDemo
//
//  Created by chyo on 15/7/29.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#define kRedirectURI @"http//:www.weibo.com"
#define APPKey  @"1907177865"


#import "ViewController.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "SsoWebservice.h"
#import "SsoUser.h"
#import "WeiboSDK.h"


@interface ViewController ()<TencentLoginDelegate, TencentSessionDelegate, NSURLConnectionDataDelegate> {
    TencentOAuth *tecentOAuth;
    NSArray * permissions;
}

@property (nonatomic, strong) NSMutableData *xData;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *adr;
@property (weak, nonatomic) IBOutlet UIButton *qqBtn;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    tecentOAuth = [[TencentOAuth alloc] initWithAppId:@"1104790130" andDelegate:self];
    //授权登陆获取的信息
    permissions = [NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil];
    self.xData = [[NSMutableData alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)qqAuthorizationClick:(id)sender {
    [tecentOAuth authorize:permissions inSafari:YES];
}

- (IBAction)weiboAuthorizationClick:(id)sender {
    [WeiboSDK enableDebugMode:YES];
    [WeiboSDK registerApp:APPKey];
    
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = kRedirectURI;
    request.scope = @"all";
    request.userInfo = @{@"myKey": @"myValue"};
    [WeiboSDK sendRequest:request];
}

- (void)getUserInfo:(NSString *)token openid:(NSString *)openid {
    NSLog(@"%@", [SsoWebservice getQQuserInfointoken:token openId:openid]);
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[SsoWebservice getQQuserInfointoken:token openId:openid]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData   timeoutInterval:1.0f];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

#pragma mark - TencentLoginDelegate
- (void)tencentDidLogin
{    
    if (tecentOAuth.accessToken && 0 != [tecentOAuth.accessToken length]) {
        //  记录登录用户的OpenID、Token以及过期时间
//        NSLog(@"%@ - %@", tecentOAuth.accessToken, tecentOAuth.openId);
        self.qqBtn.hidden = true;
        [tecentOAuth getUserInfo];
//        [self getUserInfo:tecentOAuth.accessToken openid:tecentOAuth.openId];
    } else {
        NSLog(@"%@", @"登录不成功 没有获取accesstoken");
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled {
    NSLog(@"用户取消");
}

/**
 * 登录时网络有问题的回调
 */
- (void)tencentDidNotNetWork {
    NSLog(@"请检查你的网络");
}

#pragma mark NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.xData appendData:data];
    NSLog(@"%@", self.xData);
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *str = [[NSString alloc] initWithData:self.xData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", str);
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:self.xData options:NSJSONReadingAllowFragments error:nil];
    SsoUser *user = [[SsoUser alloc] init];
    [user setValuesForKeysWithDictionary:dict];
    self.headImg.layer.cornerRadius = 37.5f;
    self.headImg.layer.masksToBounds = YES;
    self.name.text = user.nickname;
    self.adr.text = [NSString stringWithFormat:@"%@  %@", user.province, user.city];
    self.headImg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:user.figureurl_qq_2]]];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


- (void)getUserInfoResponse:(APIResponse*) response {
    SsoUser *user = [[SsoUser alloc] init];
    [user setValuesForKeysWithDictionary:response.jsonResponse];
    NSLog(@"%@", user);
    self.headImg.layer.cornerRadius = 37.5f;
    self.headImg.layer.masksToBounds = YES;
    self.name.text = user.nickname;
    self.adr.text = [NSString stringWithFormat:@"%@  %@", user.province, user.city];
    self.headImg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:user.figureurl_qq_2]]];
//    NSLog(@"%@", res)
}




@end
