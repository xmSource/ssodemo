//
//  SsoUser.m
//  SsoDemo
//
//  Created by chyo on 15/7/30.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import "SsoUser.h"

@implementation SsoUser

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"nickname"]) {
        self.nickname = value;
    } else if ([key isEqualToString:@"figureurl_qq_2"]) {
        self.figureurl_qq_2 = value;
    } else if([key isEqualToString:@"city"]) {
        self.city = value;
    } else if ([key isEqualToString:@"province"]) {
        self.province = value;
    }
}

@end
