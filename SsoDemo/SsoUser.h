//
//  SsoUser.h
//  SsoDemo
//
//  Created by chyo on 15/7/30.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SsoUser : NSObject

@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *figureurl_qq_2;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *province;

- (void)setValue:(id)value forUndefinedKey:(NSString *)key;
- (instancetype)initWithDict:(NSDictionary *)dict;

@end
