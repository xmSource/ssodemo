//
//  AppDelegate.h
//  SsoDemo
//
//  Created by chyo on 15/7/29.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

