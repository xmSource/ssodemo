//
//  SsoWebservice.m
//  SsoDemo
//
//  Created by chyo on 15/7/30.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import "SsoWebservice.h"

@implementation SsoWebservice

+ (NSString *)getQQuserInfointoken:(NSString *)Token openId:(NSString *)openid {
    return [@"https://graph.qq.com/user/get_user_info?" stringByAppendingFormat:[NSString stringWithFormat:@"oauth_consumer_key=%@&access_token=%@&openid=%@", APPID, Token, openid]];
}

@end
